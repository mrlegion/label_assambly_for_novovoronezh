﻿using System.Windows;
using System.Windows.Controls;
using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LabelApp.View;

namespace LabelApp
{
    public class ShellViewModel : ViewModelBase
    {
        private Page content;

        public ShellViewModel()
        {
            Content = ServiceLocator.Current.GetInstance<MainView>();
        }

        public Page Content
        {
            get => this.content;
            set => Set(() => Content, ref this.content, value);
        }
    }
}