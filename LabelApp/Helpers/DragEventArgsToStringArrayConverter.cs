﻿using System.Windows;
using GalaSoft.MvvmLight.Command;

namespace LabelApp.Helpers
{
    public class DragEventArgsToStringArrayConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            var args = (DragEventArgs) value;
            string[] result = null;
            if (args.Data.GetDataPresent(DataFormats.FileDrop))
            {
                result = (string[]) args.Data.GetData(DataFormats.FileDrop);
            }

            return result ?? new string[] {};
        }
    }
}