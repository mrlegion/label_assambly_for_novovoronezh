﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using LabelApp.Model.Common;

namespace LabelApp.Helpers
{
    public class IntToRotationValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var obj = value as ComboBoxItem;
            if (obj == null)
                return RotationValue.None;

            if (int.TryParse(obj.Tag.ToString(), out int number))
            {
                return (RotationValue) number;
            }

            return RotationValue.None;
        }
    }
}