﻿using System.Windows;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using LabelApp.Model.Implimentations;
using LabelApp.Model.Interfaces;
using LabelApp.View;
using LabelApp.ViewModel;

namespace LabelApp
{
    public class Bootstrapper
    {
        public Bootstrapper()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
        }

        public void Run()
        {
            RegisterView();
            RegisterViewModel();
            RegisterComponent();

            InitMainWindow();
        }

        private void InitMainWindow()
        {
            Application.Current.MainWindow = ServiceLocator.Current.GetInstance<ShellView>();
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            Application.Current.MainWindow?.Show();
        }

        private void RegisterComponent()
        {
            SimpleIoc.Default.Register<ICollector, Collector>();
        }

        private void RegisterView()
        {
            //SimpleIoc.Default.Register<MainWindow>();
            SimpleIoc.Default.Register<ShellView>();
            SimpleIoc.Default.Register<MainView>();
        }

        private void RegisterViewModel()
        {
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<ShellViewModel>();
        }
    }
}