﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using LabelApp.Model.Common;
using LabelApp.Model.Interfaces;
using LabelApp.View;
using MaterialDesignThemes.Wpf;
using Microsoft.WindowsAPICodePack.Dialogs;
namespace LabelApp.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private RotationValue rotation;
        private ObservableCollection<SelectFile> files;
        private string saveFolderPath;
        private string oldSaveFolderPath;

        public MainViewModel()
        {
            Messenger.Default.Register<NotificationMessageAction<bool>>(this, MessangerHandler);
        }

        private async void MessangerHandler(NotificationMessageAction<bool> obj)
        {
            var view = new FileExistDialogView();
            var result = await DialogHost.Show(view, "RootDialog");
            obj.Execute(result);
        }

        public RelayCommand ExitCommand
        {
            get { return new RelayCommand(() => Application.Current.Shutdown()); }
        }

        public RotationValue Rotation
        {
            get => this.rotation;
            set { Set(() => Rotation, ref this.rotation, value); }
        }

        public ReadOnlyObservableCollection<SelectFile> Files =>
            new ReadOnlyObservableCollection<SelectFile>(this.files ?? (this.files = new ObservableCollection<SelectFile>()));

        public string SaveFolderPath
        {
            get => this.saveFolderPath;
            set { Set(() => SaveFolderPath, ref this.saveFolderPath, value); }
        }

        public RelayCommand SelectFolderCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (string.IsNullOrEmpty(this.oldSaveFolderPath))
                        this.oldSaveFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);

                    CommonOpenFileDialog dialog = new CommonOpenFileDialog
                    {
                        Multiselect = false,
                        InitialDirectory = this.oldSaveFolderPath,
                        AllowNonFileSystemItems = true,
                        Title = "Выберете директорию для сохранения",
                        IsFolderPicker = true
                    };

                    if (dialog.ShowDialog() != CommonFileDialogResult.Ok)
                        return;

                    this.oldSaveFolderPath = Path.GetDirectoryName(dialog.FileName);
                    SaveFolderPath = dialog.FileName;
                });
            }
        }

        public RelayCommand AddingFileCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    CommonOpenFileDialog dialog = new CommonOpenFileDialog
                    {
                        Multiselect = true,
                        InitialDirectory = this.oldSaveFolderPath,
                        AllowNonFileSystemItems = true,
                        Filters = { new CommonFileDialogFilter("Pdf файлы", "pdf") },
                        Title = "Выберете файлы с наклейками",
                        IsFolderPicker = false
                    };

                    if (dialog.ShowDialog() != CommonFileDialogResult.Ok)
                        return;

                    if (!dialog.FileNames.Any())
                        return;

                    foreach (string name in dialog.FileNames)
                    {
                        this.files.Add(new SelectFile() { Path = name });
                    }
                });
            }
        }

        public RelayCommand ClearListCommand
        {
            get
            {
                return new RelayCommand(() => { this.files.Clear(); });
            }
        }

        public RelayCommand AssemblyCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (Files.Count == 0)
                        return;

                    if (string.IsNullOrWhiteSpace(SaveFolderPath) || !Directory.Exists(SaveFolderPath))
                        SaveFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                    var collector = SimpleIoc.Default.GetInstance<ICollector>();
                    collector.CollectPage(Files, SaveFolderPath, Rotation);
                });
            }
        }

        public RelayCommand<string[]> DropCommand
        {
            get { return new RelayCommand<string[]>(s =>
            {
                if (s.Length > 0)
                {
                    var pdf = s.Where(path => path.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase));
                    foreach (string path in pdf)
                        this.files.Add(new SelectFile() { Path = path });
                }
            });}
        }
    }
}