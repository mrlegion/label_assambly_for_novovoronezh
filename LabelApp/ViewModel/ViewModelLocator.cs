﻿using CommonServiceLocator;

namespace LabelApp.ViewModel
{
    public class ViewModelLocator
    {
        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();

        public ShellViewModel Shell => ServiceLocator.Current.GetInstance<ShellViewModel>();
    }
}