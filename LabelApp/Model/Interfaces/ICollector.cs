﻿using System.Collections.Generic;
using iText.Kernel.Pdf;
using LabelApp.Model.Common;

namespace LabelApp.Model.Interfaces
{
    public interface ICollector
    {
        void CollectPage(IEnumerable<SelectFile> collection, string path);
        void CollectPage(IEnumerable<SelectFile> collection, string path, RotationValue rotationValue);
    }
}