﻿namespace LabelApp.Model.Common
{
    public class SelectFile
    {
        public string Name
        {
            get { return string.IsNullOrEmpty(Path) 
                    ? "Безымяный" 
                    : System.IO.Path.GetFileNameWithoutExtension(Path); }
        }

        public string Path { get; set; }
        public int Count { get; set; }
    }
}