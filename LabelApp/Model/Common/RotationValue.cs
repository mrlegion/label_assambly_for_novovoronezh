﻿namespace LabelApp.Model.Common
{
    public enum RotationValue
    {
        None = 0,
        Horizontal = 1,
        Vertical = 2
    }
}