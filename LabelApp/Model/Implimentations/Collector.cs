﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using iText.Kernel.Pdf;
using LabelApp.Model.Common;
using LabelApp.Model.Interfaces;
using LabelApp.ViewModel;

namespace LabelApp.Model.Implimentations
{
    public class Collector : ICollector
    {
        private FileInfo saveFileInfo;
        private List<SelectFile> list;
        private RotationValue rotation;
        
        public void CollectPage(IEnumerable<SelectFile> collection, string path)
        {
            CollectPage(collection, path, RotationValue.None);
        }

        public void CollectPage(IEnumerable<SelectFile> collection, string path, RotationValue rotationValue)
        {
            if (collection == null)
                return;

            this.saveFileInfo = new FileInfo(Path.Combine(path, "собранные ярлыки.pdf"));

            this.list = collection.ToList();

            this.rotation = rotationValue;

            if (this.saveFileInfo.Exists)
            {
                var message = new NotificationMessageAction<bool>(this, "Файл уже существует!", result =>
                {
                    if (result)
                        CollectOnNewFile();
                    else
                        CollectOnNewFile(Path.GetRandomFileName());
                });

                Messenger.Default.Send(message);
                return;
            }

            CollectOnNewFile();
        }

        private void CollectOnNewFile(string name)
        {
            var newFile = Path.Combine(this.saveFileInfo.DirectoryName ?? Environment.GetFolderPath(Environment.SpecialFolder.Desktop), name + ".pdf");
            this.saveFileInfo = new FileInfo(newFile);
            CollectOnNewFile();
        }

        private void CollectOnNewFile()
        {
            var saveDocument = new PdfDocument(new PdfWriter(this.saveFileInfo.FullName));

            foreach (SelectFile file in this.list)
            {
                if (!File.Exists(file.Path))
                    throw new FileNotFoundException(file.Path);

                var document = new PdfDocument(new PdfReader(file.Path));
                
                if (this.rotation != RotationValue.None)
                    CheckOrientationPage(document.GetFirstPage());

                for (int i = 0; i < file.Count; i++)
                {
                    document.CopyPagesTo(1, 1, saveDocument);
                }

                document.Close();
            }

            saveDocument.Close();
        }

        private void CheckOrientationPage(PdfPage page)
        {
            var size = page.GetPageSize();
            switch (this.rotation)
            {
                case RotationValue.Horizontal:
                    if (size.GetWidth() < size.GetHeight())
                        page.SetRotation(270);
                    break;
                case RotationValue.Vertical:
                    if (size.GetWidth() > size.GetHeight())
                        page.SetRotation(90);
                    break;
                case RotationValue.None:
                default: return;
            }
    }
    }
}